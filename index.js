'use strict';
//import fetch from 'node-fetch';
const fetch = require('node-fetch');
const doc = require('dynamodb-doc');
const dynamo = new doc.DynamoDB();

exports.handler = async (event, context) => {
  let response;
  try {
    const waitTimes = await getWaitTimes();
    response = {
      statusCode: 200,
      body: JSON.stringify(waitTimes),
      //{
      //message: 'hello world',
      // location: ret.data.trim()
      //}
      //)
    };
  } catch (err) {
    console.log(err);
    return err;
  }

  return response;
};

async function getWaitTimes() {
  let url = 'https://epicapi.overlakehospital.org/waittime';
  let ret = await fetch(url);
  let jsonData = await ret.json();
  console.log(jsonData);
  return jsonData;
}

/**
 * Demonstrates a simple HTTP endpoint using API Gateway. You have full
 * access to the request and response payload, including headers and
 * status code.
 *
 * To scan a DynamoDB table, make a GET request with the TableName as a
 * query string parameter. To put, update, or delete an item, make a POST,
 * PUT, or DELETE request respectively, passing in the payload to the
 * DynamoDB API as a JSON body.
 */
function saveWaitTimes(wtJson) {
  //console.log('Received event:', JSON.stringify(event, null, 2));

  switch (event.httpMethod) {
    case 'DELETE':
      dynamo.deleteItem(JSON.parse(event.body), done);
      break;
    case 'GET':
      dynamo.scan({ TableName: event.queryStringParameters.TableName }, done);
      break;
    case 'POST':
      dynamo.putItem(JSON.parse(event.body), done);
      break;
    case 'PUT':
      dynamo.updateItem(JSON.parse(event.body), done);
      break;
    default:
      done(new Error(`Unsupported method "${event.httpMethod}"`));
  }
}
